#!/bin/bash

# Install nix on the job
# ./scripts/install_nix.sh

# Load nix
. /home/afaure/.nix-profile/etc/profile.d/nix.sh

# Install the environment and gives a link to the closure
./scripts/install_env.sh paralleviEnv /home/afaure/parEnv06.tar.bz2

# We save the closure for the next time
build_nix_closure.sh -f myPkgs -e paralleviEnv -o parEnv06.tar.bz2

# url of spark master with the port
start_spark.sh

spark_master=$(cat /tmp/master-spark-afaure)
echo $spark_master

# I could split this part
# Curie
(cd /home/afaure/parallevi/experiments/;
spark-submit --class "BatSpark" --master \
  spark://$spark_master \
  job_spark/target/scala-2.11/batspark_2.11-1.0.jar \
  exp_curie/inputs_50_usage.csv \
  data/Curie
)

(cd /home/afaure/parallevi/experiments/;
spark-submit --class "BatSpark" --master \
  spark://$spark_master \
  job_spark/target/scala-2.11/batspark_2.11-1.0.jar \
  exp_curie/inputs_70_usage.csv \
  data/Curie-70
)

# MUSTANG
(cd /home/afaure/parallevi/experiments/;
spark-submit --class "BatSpark" --master \
  spark://$spark_master \
  job_spark/target/scala-2.11/batspark_2.11-1.0.jar \
  exp_mustang/inputs_50_usage.csv \
  data/LANL-Mustang
)

(cd /home/afaure/parallevi/experiments/;
spark-submit --class "BatSpark" --master \
  spark://$spark_master \
  job_spark/target/scala-2.11/batspark_2.11-1.0.jar \
  exp_mustang/inputs_70_usage.csv \
  data/LANL-Mustang-70
)

# Intrepid
(cd /home/afaure/parallevi/experiments/;
spark-submit --class "BatSpark" --master \
  spark://$spark_master \
  job_spark/target/scala-2.11/batspark_2.11-1.0.jar \
  exp_intrepid/inputs_50_usage.csv \
  data/ANL-Intrepid
)

(cd /home/afaure/parallevi/experiments/;
spark-submit --class "BatSpark" --master \
  spark://$spark_master \
  job_spark/target/scala-2.11/batspark_2.11-1.0.jar \
  exp_intrepid/inputs_70_usage.csv \
  data/ANL-Intrepid-70
)
