#/usr/bin/env bash

find $1 -iname '*json' -exec cp -r \{\} . \;
find $1 -iname '*xml' -exec cp -r \{\} . \;
