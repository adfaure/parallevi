name := "batspark"

version := "1.0"

scalaVersion := "2.11.8"
scalacOptions := Seq("-unchecked", "-deprecation")
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.1"
