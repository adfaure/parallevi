import java.io.File

import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.SparkFiles
import org.apache.spark.TaskContext
import org.apache.spark.Partitioner
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.types._
import java.util.UUID.randomUUID
import scala.io.Codec
import org.apache.hadoop.io.compress.BZip2Codec

object BatSpark {

  // Helper to get the list of all files in a directory
  def getListOfFiles(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
        d.listFiles.filter(_.isFile).toList
    } else {
        List[File]()
    }
  }

  // Helper to add all file from a directory to
  // a the context.
  def addFolderToContext(sc: SparkContext, dir: String) = {
    getListOfFiles(dir).foreach(
      p => sc.addFile(p.toString)
    )
  }

  // Function to customize the way the paramters are
  // given to the forked process (robin in our case).
  // This function print the value for the key.
  def printRDDElement(elem: (Any, String), f: String => Unit ) = {
    println(elem._2)
    f(elem._2)
  }

  def main(args: Array[String]) {

    val inputcsv = args.length match {
      case x: Int if x > 0 => args(0)
      case _ => "local"
    }

    val datafolder = args.length match {
      case x: Int if x > 1 => args(1)
      case _ => "local"
    }

    // The spark session
    val spark = SparkSession
    .builder()
    .appName("Batsim")
    .getOrCreate()

    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext

    import sqlContext.implicits._

    // We define the schema by
    // hands otherwise, spark detects
    // the id as int, and I want to keep
    // the 0 before the string.
    val schema = StructType(
      $"id".string ::
      $"socket".int ::
      $"type".string ::
      $"platform".string ::
      $"workload".string ::
      $"percentage-allocation".double ::
      $"threshold".int ::
      $"conflicts_resolution_policy".string ::
      $"threshold_filter_policy".string ::
      $"threshold_policy".string :: Nil
    )

    val workloads = datafolder + "/workloads"

    // Add the data folder, so it can be loaded by all nodes
    sc.addFile("scripts/compute_stretches.R")
    addFolderToContext(sc, datafolder)
    addFolderToContext(sc, workloads)

    // We load the csv as a spark rdd
    val inputsRDD = spark.read.format("csv")
      .schema(schema)
      .option("header", "true")
      .load(inputcsv)

    // We specify a folder to gather the results
    val uuid = randomUUID().toString
    val resultsFolderMetrics = uuid + "/metrics"

    val simulations = inputsRDD
      .collect()
      // The RDD is mapped as pairRDD to be able to
      // use the function partitionBy
      .map(r => {
        new String(r.mkString("",",",""))
      })

    val results = sc.makeRDD(simulations)
    .pipe(Seq("/bin/bash", "run_robin.sh"),
      Map(),
      null,
      null,
      false,
      8024,
      Codec.defaultCharsetCodec.name)

    // We gather the metrics written by batsim
    val metrics = results.filter(k => k.startsWith("robid:"))
    .map(r => r.split("robid:").tail.head)
    .saveAsTextFile(resultsFolderMetrics, classOf[BZip2Codec])

    sc.stop()
  }
}
