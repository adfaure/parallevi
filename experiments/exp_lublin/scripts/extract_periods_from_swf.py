#!/bin/env python

"""
This script extract periods of time
from a given swf file. The strength of this
script is that it uses evalys, to allow to
extract only periods, that has a mean utilization
specified as entry.

The script is dependent to evalys. And it has been created
to be used in conjunction with nix. To use the script,
one can just install it with nix.

The following environment contains, this script and evalys
in the same env, so it would be ready to use.

You just need to dl the
repository: https://gitlab.inria.fr/adfaure/myPkgs

`nix-env -iA expEnv -f ~/Projects/myPkgs`

Usage:
    extract_weeks.py <swf_file> <number_of_week> <period> <mean_util> <results_dir>

Options:
  -h --help     Show this screen.
  --version     Show version.
"""
import sys
from docopt import docopt
from evalys import workload
from evalys.metrics import compute_load, load_mean
import pandas as pd
import os
import numpy as np


def onthefly_extract_periods_with_given_utilisation(w,
                                           period_in_hours,
                                           utilisation,
                                           variation=0.01,
                                           shift_window=None,
                                           nb_max=None,
                                           merge_basic=False,
                                           merge_change_submit_times=False,
                                           randomize_starting_times=False,
                                           random_seed=0,
                                           max_nb_jobs=None):
    '''
    This extract from the workload a period (in hours) with a given mean
    utilisation (between 0 and 1).
    :returns:
        a list of workload of the given periods, with the given
        utilisation, extracted from the this workload.

    '''
    norm_util = w.utilisation

    if shift_window is None:
        shift_window = period_in_hours

    # resample the dataframe with the given period
    if randomize_starting_times:
        np.random.seed(random_seed)
        c = np.random.choice(norm_util.index, size=50)
        time_periods = np.compress(
            c <= max(norm_util.index) - period_in_hours * (60 * 60), c)
    else:
        time_periods = np.arange(min(norm_util.index),
                                 max(norm_util.index),
                                 60 * 60 * shift_window)
    mean_df = pd.DataFrame()
    load_df = norm_util.reset_index()
    max_to = max(load_df.time)

    for index, val in enumerate(time_periods):
        if index == len(time_periods) - 1 or index == 0:
            continue
        begin = val

        end = val + period_in_hours * (60 * 60)
        if end > max_to:
            end = max_to

        mean_df = mean_df.append(
            {"begin": begin,
             "end": end,
             "mean_util": load_mean(norm_util,
                                    begin=begin,
                                    end=end)},
            ignore_index=True)

    mean_df["norm_mean_util"] = mean_df.mean_util / w.MaxProcs

    periods = mean_df.loc[
        lambda x: x.norm_mean_util >= (utilisation - variation)
    ]

    # Only take nb_max periods if it is defined
    if nb_max:
        periods = periods[:nb_max]

    notes = ("Period of {} hours with a mean utilisation "
             "of {}".format(period_in_hours, utilisation))
    return w.extract(periods, notes, merge_basic=merge_basic,
                        merge_change_submit_times=merge_change_submit_times,
                        max_nb_jobs=max_nb_jobs)

def extract_periods(w, period, results_dir, number_of_week,
                    util):

    variation = 0.2

    os.makedirs(results_dir, exist_ok=True)

    columns = ["file", "begin", "end", "norm_util",
               "variation", "period_in_hours"]
    metadata_map = pd.DataFrame(columns=columns)

    #TODO the shift windows should an input para
    res_table = onthefly_extract_periods_with_given_utilisation(
        w, period, util / 10, variation=variation, nb_max=number_of_week, shift_window=168)

    get_some = False
    for i, results in enumerate(res_table):
        get_some = True
        filename = "extracted_{}H_{}util+-{}_{}".format(
            period, util * 10,
            variation,
            i)
        # print("Export: {} \n{}".format(filename, results))
        filepath = results_dir + "/" + filename + ".swf"
        results.to_csv(filepath)

        # Add metadata
        metadata_map = metadata_map.append(
            [{"file": filename,
                "begin": results.ExtractBegin,
                "end": results.ExtractEnd,
                "norm_util": util / 10,
                "variation": variation,
                "period_in_hours": period}])

    return metadata_map


if __name__ == "__main__":
    arguments = docopt(__doc__, version='Gen Weeks 0.0.1')

    results_dir = arguments['<results_dir>']
    swf_file = arguments['<swf_file>']

    number_of_week = arguments["<number_of_week>"]
    period = arguments["<period>"]
    mean_util = arguments["<mean_util>"]

    w = workload.Workload.from_csv(swf_file)
    extract_periods(w,
                    int(period),
                    results_dir,
                    int(number_of_week),
                    float(mean_util)
                    ).to_csv(sys.stdout, header=False, index=False)
