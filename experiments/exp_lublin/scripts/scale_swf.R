library(dplyr)

# arg1 originalcsv, arg2 output
args <-commandArgs(TRUE)

workload_file <- args[1]
number_of_machines <- args[2]
allocation <- as.numeric( args[3])
output_file  <- args[4]

scaleWorkload <- function(workload_file, output, scaling_percentage, number_of_machines) {
  # In the first place we load the swf file
  workload <- read.csv(workload_file, header=FALSE, sep="", comment.char=";")

  # We apply a header to facilitate the manipulations
  colnames(workload) <- c(
  "JobNumber",
  "SubmitTime",
  "WaitTime",
  "RunTime",
  "NumberOfAllocatedProcessors",
  "AverageCPUTimeUsed",
  "UsedMemory",
  "RequestedNumberOfProcessors",
  "RequestedTime",
  "RequestedMemory",
  "Status",
  "UserID",
  "GroupID",
  "ExeutableApplicationNumber",
  "QueueNumber",
  "PartitionNumber",
  "PrecedingJobNumber",
  "ThinkTimefromPreceding")

  workload = workload %>% rowwise() %>%
    mutate(PercentageOfAllocatedProcessors = NumberOfAllocatedProcessors/number_of_machines) %>%
    mutate(NumberOfAllocatedProcessors = max(1, floor(scaling_percentage * NumberOfAllocatedProcessors)-1)) %>%
    mutate(RequestedTime = ceiling(RequestedTime * (1/scaling_percentage))) %>%
    # mutate(RunTime = ceiling(RunTime * (1/scaling_percentage))) %>%
    # mutate(RequestedTime = RunTime) %>%
    # mutate(SubmitTime = ceiling(SubmitTime * (1/scaling_percentage)))

  write.table(workload, output,
              sep=" ",
              row.names=FALSE,
              col.names=FALSE)
}


scaleWorkload(workload_file, output_file, (1-allocation), strtoi(number_of_machines))
